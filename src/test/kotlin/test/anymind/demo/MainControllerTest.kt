package test.anymind.demo

import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@SpringBootTest
@AutoConfigureMockMvc
class MainControllerTest {
    @Autowired
    lateinit var webApplicationContext: WebApplicationContext
    lateinit var mockMvc: MockMvc

    @BeforeEach
    fun init() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    }

    @Test
    fun `List all wallet history`() {
        this.mockMvc.perform(get("/"))
            .andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$").isArray)
            //.andExpect(jsonPath("$", hasSize<WalletHistory>(greaterThan(0))))
    }

    @Test
    fun `Success add transaction to wallet history`() {
        val payloadJson = """{
            "datetime": "2019-10-05T14:48:01+01:00",
            "amount": 1.1
        }""".trimIndent()

        mockMvc.perform(post("/save")
            .content(payloadJson)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isCreated)
            .andExpect(jsonPath("$.datetime", `is`("2019-10-05T13:48:01.000+00:00")))
            .andExpect(jsonPath("$.amount", `is`(1.1)))
    }

    @Test
    fun `Add transaction without date`() {
        val payloadJson = """{
            "amount": 1.1
        }""".trimIndent()

        mockMvc.perform(post("/save")
            .content(payloadJson)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().is4xxClientError)
            //.andExpect(jsonPath("$.errors").isArray)
            //.andExpect(jsonPath("$.errors", hasSize<WalletHistory>(1)))
            //.andExpect(jsonPath("$.errors[0]").value("No datetime"))
    }

    @Test
    fun `Add transaction without amount`() {
        val payloadJson = """{
            "datetime": "2019-10-05T14:48:01+01:00"
        }""".trimIndent()

        mockMvc.perform(post("/save")
            .content(payloadJson)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().is4xxClientError)
            //.andExpect(jsonPath("$.errors").isArray)
            //.andExpect(jsonPath("$.errors", hasSize<WalletHistory>(1)))
            //.andExpect(jsonPath("$.errors[0]").value("No amount"))
    }

    @Test
    fun `Add transaction with negative amount`() {
        val payloadJson = """{
            "datetime": "2019-10-05T14:48:01+01:00",
            "amount": -9.1
        }""".trimIndent()

        mockMvc.perform(post("/save")
            .content(payloadJson)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().is4xxClientError)
        //.andExpect(jsonPath("$.errors").isArray)
        //.andExpect(jsonPath("$.errors", hasSize(1)))
        //.andExpect(jsonPath("$.errors[0]").value("Must be positive number"))
    }

    @Test
    fun `Add transaction with invalid date format`() {
        val payloadJson = """{
            "datetime": "2019/10/05 14:48:01",
            "amount": 1.1
        }""".trimIndent()

        mockMvc.perform(post("/save")
            .content(payloadJson)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().is4xxClientError)
        //.andExpect(jsonPath("$.errors").isArray)
        //.andExpect(jsonPath("$.errors", hasSize(1)))
        //.andExpect(jsonPath("$.errors[0]").value("Datetime must be in format..."))
    }
}