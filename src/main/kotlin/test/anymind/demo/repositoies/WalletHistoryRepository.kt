package test.anymind.demo.repositoies

import org.springframework.data.mongodb.repository.Aggregation
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import test.anymind.demo.models.WalletHistory
import java.util.*


@Repository
interface WalletHistoryRepository : CrudRepository<WalletHistory, Long> {
    @Aggregation(pipeline = [
        "{\$group:{ _id: {\$toDate: { \$subtract : ['\$datetime',{ \$mod: [{\$toLong:'\$datetime'}, 3600000] }]}}, total: { \$sum: '\$amount' }}}",
        "{\$setWindowFields: { sortBy: { _id: 1 }, output: { amount: {\$sum: \$total, window: { documents: [ \"unbounded\", \"current\" ] }}}}}",
        "{\$project: { _id:0, datetime: '\$_id', amount: 1 }}",
        "{\$match:{datetime : { \$gte: ?0, \$lte: ?1 }}}",
    ])
    fun findHourlyAmountByDate(from: Date?, to: Date?): List<WalletHistory?>
}