package test.anymind.demo.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Description
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import test.anymind.demo.models.ViewHistoryRequest
import test.anymind.demo.models.WalletHistory
import test.anymind.demo.services.WalletHistoryService
import javax.validation.Valid


@RestController
class MainController {

    @Autowired
    lateinit var service: WalletHistoryService

    @GetMapping
    @Description("Get all wallet balance history")
    fun list(): ResponseEntity<MutableIterable<WalletHistory?>>{
        val histories: MutableIterable<WalletHistory?> = service.allHistory()
        return ResponseEntity.ok(histories)
    }

    @PostMapping("/view")
    @Description("Get each hour wallet balance")
    fun view(@Valid @RequestBody body: ViewHistoryRequest): ResponseEntity<List<WalletHistory?>> {
        val filteredHistory: List<WalletHistory?> = service.findHourlyHistory(body.startDatetime, body.endDatetime)
        return  ResponseEntity.ok(filteredHistory)
    }

    @PostMapping("/save")
    @Description("Save wallet history record")
    fun save(@Valid @RequestBody body: WalletHistory): ResponseEntity<WalletHistory> {
        val createdHistory = service.createHistory(body)
        return ResponseEntity.status(HttpStatus.CREATED).body(createdHistory)
    }
}

