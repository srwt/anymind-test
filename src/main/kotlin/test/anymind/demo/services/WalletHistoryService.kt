package test.anymind.demo.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import test.anymind.demo.repositoies.WalletHistoryRepository
import test.anymind.demo.models.WalletHistory
import java.util.*


@Service
    class WalletHistoryService @Autowired constructor(repository: WalletHistoryRepository) {
        private val repository: WalletHistoryRepository

        init {
            this.repository = repository
        }

        fun allHistory(): MutableIterable<WalletHistory> {
            return repository.findAll()
        }

        fun createHistory(history: WalletHistory): WalletHistory {
            return repository.save(history)
        }

        fun findHourlyHistory(startDate: Date, endDate: Date): List<WalletHistory?> {
            return repository.findHourlyAmountByDate(startDate, endDate)
        }
    }