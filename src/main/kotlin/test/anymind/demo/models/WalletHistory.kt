package test.anymind.demo.models

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bson.types.Decimal128
import org.bson.types.ObjectId
import org.jetbrains.annotations.NotNull
import org.springframework.data.annotation.Id
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.validation.constraints.Positive


//@Document
data class WalletHistory(

    @JsonIgnore
    @Id
    val id: ObjectId = ObjectId.get(),
    @NotNull
    @field:Positive(message = "Must be positive number")
    val amount: Decimal128,
    @NotNull
    @field:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val datetime: Date
)
