package test.anymind.demo.models

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.format.annotation.DateTimeFormat
import java.util.Date

data class ViewHistoryRequest  @JsonCreator constructor(
    @field:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    val startDatetime: Date,
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @field:DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val endDatetime: Date
)
