# Test Project
<!--![image info](./to_the_moon_img.jpg)-->
## Setup
- Clone this project
- Make sure you have Java 8, JDK 1.8 and Maven installed
- Change MongoDB configuration to your server in application.properties
- Run the following command to start app:

```bash
mvn spring-boot:run
```